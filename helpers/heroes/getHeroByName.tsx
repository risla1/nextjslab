// import React from 'react'
import {heroes} from '../../data/heroes';

export const getHeroByName = (name='') => {
    console.log("renderizado consulta")
 
    const {search} = name;

    if (!search || search.length <= 0 ){
        return[]
    };

    search =  search.toLowerCase() ;

     return heroes.filter (hero => hero.superhero.toLowerCase().includes(search)) ; 
 
}
