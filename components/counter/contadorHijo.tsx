import React, { memo } from 'react'
 import Container from 'react-bootstrap/Container';
import { Alert, Button, Row, Col, InputGroup, FormControl, Form } from 'react-bootstrap';

const ContadorHijo = ({title,contador="12",sumar,restar}) => {

  console.log("renderizado inicial");

  

  return (
    <Container>

      <Row className="d-flex flex-row justify-content-between flex-nowrap">
            <Col>
              <Alert variant="info">{title}:{contador}</Alert> 
            </Col>
        </Row>

        <Row className="d-flex flex-row justify-content-between flex-nowrap">
            <Col>
                <Button variant="success" onClick={sumar}>+</Button>
                <Button variant="info" onClick={restar}>-</Button>
            </Col>
        </Row>

    </Container>
  )
}

export default memo(ContadorHijo);

