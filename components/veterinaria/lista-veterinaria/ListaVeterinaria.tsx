import clsx from 'clsx';   
import Styles from './styles.module.scss';
import React  from 'react'; 
import { Alert, Col, Container, Row, } from 'react-bootstrap'; 
import ListvetItem from './ListvetItem';
import Accordion from 'react-bootstrap/Accordion';


const ListaVeterinaria = ( {title,data,handler} ) => {  

   
   
   const handlerChild= (id) =>{ 

      !id?   console.log("vacio desde nieto") : handler(id); 
 
   }; 




   return (
      <Container className={clsx('mt-3' )} fluid >
         <Row>
            <Col>
               <Alert variant="success" className={clsx('text-center' )}>
                  <Alert.Heading> {title} </Alert.Heading>
               </Alert>
            </Col>
         </Row>
         <Row  >
            <Accordion  >
                  
               {data.map((item, index) => { 
                  return (
                     <ListvetItem
                        id={item.id}
                        fullname={item.fullname} 
                        date={item.date} 
                        symtoms={item.symtoms}   
                        deleteChild={handlerChild}
                        key={index}
                     />
                  ); 
 
               })}

            </Accordion>
         </Row>
      </Container>
   )
   
} 
 
export default ListaVeterinaria;
