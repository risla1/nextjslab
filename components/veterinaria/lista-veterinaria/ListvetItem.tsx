   
import React  from 'react';  
import { Button, Stack } from 'react-bootstrap';
import Accordion from 'react-bootstrap/Accordion'


export interface ListvetItemProps {
	fullname: string; 
	keyid: string;  
	symtoms: string; 
	date: string;     
 }


const ListvetItem = ( {  deleteChild,   id,fullname,date,symtoms}) => {  


      
 
   function deleteRow (id) { 
      deleteChild(id);
   }


 
   
   return (
      <Accordion.Item     eventKey={id}   id={id}> 
                  <Button variant="danger" onClick={(e) => deleteRow(id)}    className="  mr-3  ">Eliminar</Button>

           <Accordion.Header>

               <Stack direction="horizontal" gap={5} className="  w-100  ">
                  <div className="bg-light border p-2 mx-auto ">{fullname} --  {date}</div>

               </Stack> 
            
            </Accordion.Header>

           <Accordion.Body>
               {symtoms}
           </Accordion.Body>
           
      </Accordion.Item>
   )
   
} 
 
export default ListvetItem;
