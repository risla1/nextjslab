import React  from 'react';
import clsx from 'clsx';   
import Styles from './styles.module.scss';
import { Alert, Container, ListGroup, Row ,Col,Form} from 'react-bootstrap'; 




const DisplayVeterinaria = ( {title ,data}  ) => {
  
 
  // obtengo el ultimo elemento de data
  const lastElement = data.slice(-1) ;  


  // if ( lastElement[0] === undefined) {
  //   console.log("valores sin definir")
  // }
  // else {
  //   const {id,fullname, email, symtoms, specie, date, time}= lastElement[0];
  //   console.log(fullname, email, symtoms, specie, date, time);
  // } 
  
 

   return (
      <Alert variant="warning" className={clsx( Styles.headerVeterinaria )} >
        <Container fluid>
            <Row className={clsx('text-center' )}>
              <h3 className={clsx(Styles.titleHeader)}> {title} </h3> 
            </Row>

          {lastElement.map((item) => { 

            return (
           
                  <Row key={item.id}>
                    <Col>
                      <ListGroup  > 
                      
                        {(item.fullname)?  
                          <ListGroup.Item>
                            <Form.Group className={clsx('mb-3')}   >
                              <Form.Label>Owner Name:</Form.Label>
                              <Form.Control type="text" defaultValue={item.fullname} readOnly/> 
                            </Form.Group> 
                          </ListGroup.Item>              
                        : ' ' }

                        {(item.email)? 
                          <ListGroup.Item>
                            <Form.Group className={clsx('mb-3')}   >
                              <Form.Label>Owner Email address</Form.Label>
                              <Form.Control type="email" defaultValue={item.email} readOnly/> 
                            </Form.Group> 
                          </ListGroup.Item>
                        : ' ' } 

                        {(item.specie)? 
                          <ListGroup.Item> 
                            <Form.Group className={clsx('mb-3')}   >
                              <Form.Label>Animal kind: </Form.Label>
                              <Form.Select  aria-label="Animal Selected" className={clsx('mb-3')} value={item.specie} disabled>
                                <option  >Animal select</option>
                                <option defaultValue="1">Cat</option>
                                <option defaultValue="2">Dog</option>
                                <option defaultValue="3">Bird</option>
                              </Form.Select>
                            </Form.Group> 
                          </ListGroup.Item>  
                        : ' ' }


                        {(item.symtoms)? 
                          <ListGroup.Item>
                            <Form.Group className={clsx('mb-3')}   >
                              <Form.Label>Symptoms Animal: </Form.Label> 
                              <Form.Control
                                as="textarea"
                                defaultValue={item.symtoms } 
                                style={{ height: '100px' }}
                                readOnly
                              />
                            </Form.Group> 
                          </ListGroup.Item> 
                        : ' ' }
        
                        {(item.date)?  
                          <ListGroup.Item>
                            <Form.Group className={clsx('mb-3')}  >
                              <Form.Label>Date</Form.Label>
                              <Form.Control
                                type="date"
                                name="duedate" 
                                defaultValue= {item.date}
                                readOnly 
                              />
                            </Form.Group> 
                          </ListGroup.Item> 
                        : ' '  }

                      
                        {(item.time)?                   
                          <ListGroup.Item>
                              <Form.Group className={clsx('mb-3')} >
                              <Form.Label>Time</Form.Label>
                              <Form.Control
                                type="time"   
                                defaultValue={ item.time }
                                readOnly
                              />
                            </Form.Group> 
                          </ListGroup.Item>                   
                        : ' ' } 
      
                      </ListGroup> 


                    </Col>  
                  </Row>  

              );
          })}
        </Container>
      </Alert>
   )
   
} 
 
export default DisplayVeterinaria;
