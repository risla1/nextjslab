import clsx from 'clsx';   
import Styles from './styles.module.scss';
import Form from 'react-bootstrap/Form';
import React  from 'react';

import { Card, Button, FormControl } from 'react-bootstrap';
import uuid from 'uuid'
 
// class Sumador extends Component {
const FormCitas = (props) => {  
     
  const fullnameRef = React.useRef(null);
  const emailRef = React.useRef(null);
  const specieRef = React.useRef(null);
  const symtomsRef = React.useRef(null);
  const dateRef = React.useRef(null);
  const timeRef = React.useRef(null);

 
  // const [date, setDate] = useState(); 
 

  const handleSubmit =(e) => {
   
    e.preventDefault();
    
    if ( fullnameRef && emailRef && specieRef ){ 
      
      // setKey(Key+1);

      const data = {  
        "id": uuid(),
        "fullname" : fullnameRef.current?.value !== null ? fullnameRef.current.value: ' ',
        "email" : emailRef.current?.value !== null ? emailRef.current.value: ' ',
        "specie" : specieRef.current?.value !== null ? specieRef.current.value: ' ',
        "symtoms" : symtomsRef.current?.value !== null ? symtomsRef.current.value: ' ',
        "date" : dateRef.current?.value !== null ? dateRef.current.value: ' ',
        "time" : timeRef.current?.value !== null ? timeRef .current.value: ' ',
      
      } ;
 
      props.sendParent(data);
      e.currentTarget.reset();

    } 

    
  } 

  return(
      <Card className={clsx( Styles.containerForm,'m-0' )} > 

              <Form className={clsx( Styles.citasForm)} onSubmit={handleSubmit}> 
                
                <Form.Group className={clsx('mb-3')}  controlId="formBasicName">
                  <Form.Label>Owner Full Name</Form.Label>
                  <Form.Control type="text" placeholder="Full Name" ref={fullnameRef} /> 
                </Form.Group> 
                
                <Form.Group className={clsx('mb-3')}  controlId="formBasicEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control type="email" placeholder="Enter email" ref={emailRef}  /> 
                </Form.Group> 

                <Form.Select aria-label="Default select example" className={clsx('mb-3')} ref={specieRef}  >
                  <option  >Animal select</option>
                  <option value="1">Cat</option>
                  <option value="2">Dog</option>
                  <option value="3">Bird</option>
                </Form.Select>                  

                <Form.Group className={clsx('mb-3')}  >
                  <Form.Label>Symtoms</Form.Label>
                  <FormControl as="textarea" aria-label="With textarea" ref={symtomsRef}/>
                </Form.Group>

                <Form.Group className={clsx('mb-3')}  >
                  <Form.Label>Date</Form.Label>
                  <Form.Control
                    type="date"
                    name="duedate"
                    placeholder="Set date"
                    ref={dateRef} 
                    // onChange={(e) => setDate(e.target.value)}
                    // value={date}

                  />
                </Form.Group> 

                <Form.Group className={clsx('mb-3')} >
                  <Form.Label>Time</Form.Label>
                  <Form.Control
                    type="time"
                    name="duedate"
                    placeholder="Set Dating Time " 
                    ref={timeRef} 
                  />
                </Form.Group>  

                <Form.Group className={clsx('mt-5 d-flex justify-content-end')}  >
                  <Button type="submit">Submit</Button>
                </Form.Group>


              </Form>

      </Card>
  )
    
}

export default FormCitas;
