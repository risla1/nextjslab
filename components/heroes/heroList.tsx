import React from 'react' 
import clsx from 'clsx';   

import { Card } from 'react-bootstrap';
import { getHeroesPublisher } from '../../helpers/heroes/geHeroesPublished'
import HeroCards from './heroCards/HeroCards';

const HeroList = ({publisher}) => {

    const heroes = getHeroesPublisher(publisher);

    return (
        <Card > 
            <h1>Hero List --- {publisher} </h1>
            <hr/>
            <ul className={clsx('d-flex  flex-row justify-content-between align-content-start flex-wrap p-0')}>
                {
                    heroes.map(hero=>(
                        <HeroCards  key={hero.id} {...hero} size={3} />
                    ) )
                }
            </ul>
             
        </Card>
    )
}

export default HeroList;

