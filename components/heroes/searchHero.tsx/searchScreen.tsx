import React, { useMemo } from 'react';
import { Alert, Button, Col, Container, Form, FormControl, InputGroup, Row } from 'react-bootstrap';
import { getHeroByName } from '../../../helpers/heroes/getHeroByName';

import { useForm } from '../../../hooks/useForm';
import HeroCards from '../heroCards/HeroCards';
import clsx from 'clsx';   
import { useRouter } from 'next/router'

// import {NextQueryParamProvider} from 'next-query-params';
// import Link from "next/link";

export const SearchScreen = () => {
  console.log("renderizado del componente")


   

  const queryString = require('query-string');


  const router = useRouter();
  const {query} = router; 
  
  const  parsed = queryString.parse(queryString.stringify(query)); 


  const [formValues, handleInputChange] = useForm({
    searchText:  parsed,
  });

 
  const {searchText} = formValues; 
 
  const heroesFiltered = useMemo( () =>  ( getHeroByName(parsed) ), [parsed]); 
 

  const handleSearch = (e) => {
    e.preventDefault(); 
     
    if (Object.entries(searchText).length !== 0 ){ 
      let url = `/heroes?search=${searchText}`;
      router.push(url, undefined );
    } 
 
  };
  
  
 

  return (
    <Container  > 
      <Row> 

        <Form onSubmit={handleSearch}>
          
          <InputGroup className="mb-3">

            <FormControl 
              aria-label="Recipient's username"
              aria-describedby="basic-addon2" 
              name="searchText"
              autoComplete="off"  
              placeholder= "Search hero"
              // value={searchText}
              onChange={ handleInputChange }        
            />

            <Button type="submit" variant="outline-secondary" id="button-addon2">
              Search
            </Button>

          </InputGroup>

        </Form>
 
      </Row>
      <Row>
          <Col> 
            Results:
            <hr/>
          </Col>
      </Row>
      <Row>
          <Col className={clsx(' d-flex justify-content-center flex-wrap')}>

              {  
                (Object.entries(parsed).length === 0 )
                  ? <Alert variant="info"> Search A Hero</Alert>
                  :  (Object.entries(heroesFiltered).length === 0 )
                    && <Alert variant="warning">Not matches:  </Alert>
                
              }


              {
                heroesFiltered.map(
                  hero=>(
                    <HeroCards  key={hero.id} {...hero } size={6} /> 
                  )
                )
              }


      
          </Col>
      </Row>
    </Container>
  )
};
