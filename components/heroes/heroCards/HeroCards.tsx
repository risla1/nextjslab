import React from 'react'
import clsx from 'clsx';   
import Link from "next/link";

import { Card, Col } from 'react-bootstrap';
import Image from 'react-bootstrap/Image'

const HeroCards = ({id,superhero,publisher,alter_ego,first_appearance,characters,size}) => {
  return (
    <Col key={id} sm={size} className={clsx('m-2')}>
       <Card   className={clsx('animate__animated animate__fadeIn')} >
            <Card.Header>{superhero}</Card.Header>
            <Card.Body>

              <Image fluid src={`/img/heroes/${id}.jpg`} alt={superhero} />

            </Card.Body>
            <Card.Footer>
              
              <h4>{alter_ego}</h4>
              <h5>{publisher}</h5>
                {
                  (alter_ego !== characters) && <p className={clsx('text-muted')} >{characters}</p>
                }

              <hr></hr>
              
              <Link href={`/heroes/${id}`}>
                <a> Read More...</a>
              </Link>
            </Card.Footer> 
       </Card>
    </Col>
  )
}

export default HeroCards;