
import React from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
 // Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/autoplay";

import { Navigation, Pagination, Mousewheel, Keyboard, Autoplay } from "swiper";

const SlidersComp = () => {

  const [slides] = React.useState([
		{
      id:1,
			photo: '/img/img1.jpg', 
		}, 
		{	
      id:2,
			photo: '/img/img2.jpg', 
		}, 
	]);


  return (
    <>
      <Swiper
        cssMode={true}
        navigation={true}
        pagination={{
          clickable: true,
        }}       
        loop={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}        mousewheel={true}
        keyboard={true}
        modules={[Autoplay, Navigation, Pagination, Mousewheel, Keyboard]}
        className="mySwiper"
      >
 
        {slides.map((item) => {
				  return (
					<SwiperSlide key={item.id}>
            <img src={item.photo} alt="" />
          </SwiperSlide>
          );
        })}

      </Swiper>
    </>
  )
}

export default SlidersComp;
