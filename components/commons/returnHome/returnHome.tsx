import clsx from 'clsx';   
import Styles from './styles.module.scss';
import Form from 'react-bootstrap/Form';
import React  from 'react';
import Link from 'next/link'

import {  Button,  Container, Row, Col, } from 'react-bootstrap'; 
 
// class Sumador extends Component {
const ReturnHome = () => {   

  return(
    <Container fluid>
      <Row>
        <Col>
          <Button variant="link" className={clsx( 'text-center w-100' )} > 
            <Link href="/">
              <a>Regresar a Inicio</a>
            </Link>
          </Button>
        </Col>
      </Row>
    </Container >
  )
    
}

export default ReturnHome;
