import clsx from 'clsx';   
import Styles from './styles.module.scss'; 
import React  from 'react';
// import { Link, NavLink } from 'react-router-dom'
import Link from "next/link"; 
import NavBar from 'react-bootstrap/Navbar';
import { Button, Container, Nav, NavDropdown } from 'react-bootstrap';

export const Navbar = () => {
   
    const handleLogout = () => {
        alert('Logout');
    }

    return ( 
        <NavBar collapseOnSelect expand="lg" bg="dark" variant="dark" className={clsx( 'd-flex   align-center ' )}>
                         
            <Nav className=" d-flex flex-row justify-content-around flex-nowrap">
                <Nav.Link   className="m-2"  href="/heroes">Comics Heroes</Nav.Link>
                <Nav.Link   className="m-2"  href="/heroes/marvelScreen">Marvel Comics</Nav.Link>
                <Nav.Link   className="m-2"  href="/heroes/dcScreen">DC Comics</Nav.Link>
            </Nav>  

            <Nav className="  d-flex flex-row  column-gap-3  ">
                    <Nav.Link   className="m-2"   href="#">Ruben</Nav.Link>                         
                    <Nav.Link   className="m-2 btn btn-success p-1 d-flex align-items-center justify-content-center"   href="/heroes/login">Logout</Nav.Link>                         
             </Nav>                             

         </NavBar>
 
    )
}