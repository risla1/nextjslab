import clsx from 'clsx';   
import Styles from './styles.module.scss';
import Form from 'react-bootstrap/Form';
import React  from 'react';

import { Card, Button, FormControl, InputGroup } from 'react-bootstrap';
import uuid from 'uuid'
 
// class Sumador extends Component {
const FormSeguros = (props ) => {  
 


  const brandRef = React.useRef(null); 
  const yearRef = React.useRef(null); 
  const basicoRef = React.useRef(null); 
  const completoRef = React.useRef(null); 
 

  const handleSubmit =(e) => {
   
    e.preventDefault();

      let plan = basicoRef.current?.checked ? 'basico': 'completo'; 

      const data = {  
        "id": uuid(), 
        "brand" : brandRef.current?.value !== null ? brandRef.current.value: ' ',  
        "year" : yearRef.current?.value !== null ? yearRef.current.value: ' ',    
        "plan":plan,
      } ;
 
      // console.log('data from form:',data);
      props.sendParent(data);
      e.currentTarget.reset();

    

    
  } 

  return(
    <Card className={clsx( Styles.containerForm,'m-0' )} > 

            <Form className={clsx( Styles.citasForm)} onSubmit={handleSubmit}> 
             
                <Form.Select aria-label="Default select " size="md" className={clsx('mb-3 mr-3')} ref={brandRef}  >
                  {props.brands.map((item, index) => (
                    <option value={item.brand} key={index}> {item.brand} </option>
                  ))}
                </Form.Select>  

                <Form.Select aria-label="Default select " size="md" className={clsx('mb-3')} ref={yearRef}  >
                  {props.years.map((item, index) => (
                    <option value={item.year} key={index}> {item.year} </option>
                  ))}
                </Form.Select>  
             
              <InputGroup className="mb-3 ">
                 
                <div key={0} className="mb-3"> 
                      <Form.Check
                        inline
                        label={`Plan Basico`}
                        name="Plan"
                        type={'radio'}
                        id={`inline-radio-1`}
                        ref={basicoRef}

                      />
                      <Form.Check
                        inline
                        label={`Plan Completo`}
                        name="Plan"
                        type={'radio'}
                        id={`inline-radio-2`}
                        ref={completoRef}
                      /> 
                </div>
                
              </InputGroup>

              

              <Form.Group className={clsx('mt-5 d-flex justify-content-end')}  >
                <Button type="submit">Submit</Button>
              </Form.Group>


            </Form>

    </Card>
  )
    
}

export default FormSeguros;
