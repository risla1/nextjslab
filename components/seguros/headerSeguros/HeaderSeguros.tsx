import clsx from 'clsx';   
import Styles from './styles.module.scss';
import React  from 'react';
import { Alert } from 'react-bootstrap';



const HeaderSeguros = ( {title} ) => { 

   return (
      <Alert variant="info" className={clsx( Styles.headerVeterinaria )} >
        <h3 className={clsx(Styles.titleHeader)}> {title} </h3>  
      </Alert>
   )
   
} 
 
export default HeaderSeguros;
