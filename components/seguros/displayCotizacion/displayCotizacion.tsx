import clsx from 'clsx';   
import Styles from './styles.module.scss';
import Form from 'react-bootstrap/Form';
import React  from 'react';

import { Card, Button, FormControl, InputGroup, Container, Row, Alert } from 'react-bootstrap';
import uuid from 'uuid'
 
// class Sumador extends Component {
const DisplayCotizacion = ({title,resultado,info}) => {  
 
  // // console.log('data',data);
  // console.log('cotizacion',resultado);
  console.log('datas',info);

  const {brand, year, plan} = info;
 
  if(!brand || !year || !plan) return null;

  return(
    <Card className={clsx( Styles.containerForm,'m-0' )} > 

      <Alert>
        <h1>{title}</h1> 
      </Alert>

      <Card.Body>
          <ul>
            <li><h4>Brand: {brand}</h4></li>
            <li><h4>Year: {year}</h4></li>
            <li><h4>Plan: {plan}</h4></li>
            <li><h4>Costo Total: {resultado}</h4></li>
          </ul>

      </Card.Body> 
    </Card>
  )
    
}

export default DisplayCotizacion;
