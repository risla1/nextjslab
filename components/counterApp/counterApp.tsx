import clsx from 'clsx';   
import Styles from './styles.module.scss';
import Container from 'react-bootstrap/Container';

// class Sumador extends Component {
const CounterApp = ({value}) => {  
     
    return(
        <Container className={clsx( Styles.CounterAppContainer,'w-100')} >
          <h3 className={clsx(Styles.CounterAppTitle)}> Counter App value: {value} </h3>    
        </Container>
    )
    
}

export default CounterApp;
