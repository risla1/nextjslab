import { useRouter } from 'next/router'
import React from 'react'
// import {heroes} from '../../data/heroes';
import {Container, Row, Card, Col, Alert} from 'react-bootstrap';
import {getHeroById } from '../../helpers/heroes/getHeroById';
import Image from 'react-bootstrap/Image'
import clsx from 'clsx';   
import ReturnHome from '../../components/commons/returnHome/returnHome';

const hero = () => {
 

  const router = useRouter(); 
  const heroid = router.query.hero; 

  
  const hero = getHeroById(heroid); 
  console.log(hero ); 

  if (!hero)   return <Alert >no hay heroe</Alert>;



  const { superhero,characters,alter_ego,id ,publisher,first_appearance} = hero;

 
  return (

    <Container> 
      <Row> 
          <Col>   
              <ReturnHome   />   
          </Col>                    
      </Row>
      <Row>

          <Col sm="12" className={clsx('d-flex justify-content-center')} > 
            <Card className={clsx('w-50')}>
                <Card.Header>{superhero}</Card.Header>
                <Card.Body>

                  <Image fluid src={`/img/heroes/${id}.jpg`} alt={hero.superhero} />

                </Card.Body>
                <Card.Footer>
                  
                  <h4>{alter_ego}</h4>
                  <h5>{publisher}</h5>
                  <p>{first_appearance}</p>
                    { 
                      (alter_ego !== characters) && <p className={clsx('text-muted')} >{characters}</p>
                    }

                  <hr></hr>  
                  
                
                </Card.Footer>   
            </Card>
          </Col>

      </Row>
    </Container
    >


  )
 


 
}

export default hero