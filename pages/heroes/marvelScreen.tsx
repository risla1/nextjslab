import clsx from 'clsx';   
import Styles from './styles.module.scss';
import Form from 'react-bootstrap/Form';
import React  from 'react';
import { Container,Row,Col } from 'react-bootstrap';
import { Navbar } from '../../components/commons/navbar/navbar';
import ReturnHome from '../../components/commons/returnHome/returnHome';
import HeroList from '../../components/heroes/heroList';

const MarvelScreen = () => {
  return (
    <Container fluid>
        <Row>
            <Col >
                <Navbar />
            </Col> 
        </Row> 
 

        <Row>
            <Col >
                <HeroList publisher={"Marvel Comics"} />
            </Col> 
        </Row>  
             


        <Row> 
            <Col>   
                <ReturnHome/>   
            </Col>                    
        </Row>
    </Container>
  )
}

export default MarvelScreen;
