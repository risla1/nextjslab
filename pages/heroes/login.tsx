import React from 'react'
import { Container,Row,Col, Button, Alert } from 'react-bootstrap';
import ReturnHome from '../../components/commons/returnHome/returnHome';
import { Navbar } from '../../components/commons/navbar/navbar';


const Login = () => {
 
  return (
    <Container fluid>
      <Row>
          <Col >
              <Navbar />
          </Col> 
      </Row> 
      <Row>
          <Col >
              <Alert className="mt-2">LoginScreen</Alert>
              <Button>Logout</Button>
          </Col> 
      </Row> 
      
      <Row> 
          <Col>   
              <ReturnHome/>   
          </Col>                    
      </Row>
    </Container>
  )
}

export default Login;

