import clsx from 'clsx';   
import HeaderVeterinaria from '../components/veterinaria/header-veterinaria/HeaderVeterinaria';
import { useState, useEffect } from 'react'; 
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'; 
import FormCitas from '../components/veterinaria/formCitas/formCitas'; 
import DisplayVeterinaria from '../components/veterinaria/displayVeterinaria/displayVeterinaria';
import ListaVeterinaria from '../components/veterinaria/lista-veterinaria/ListaVeterinaria';
import ReturnHome from '../components/commons/returnHome/returnHome';

// class Sumador extends Component {
// const Veterinaria = () => { 
const Veterinaria = () => { 


    const [cita, setCita] = useState( [ ] );
 
    useEffect(() => {
        // console.log("entrando useefect" )  
    }, [cita]);

    const createCita= (data) =>{ 
        setCita([ ...cita, {...data} ]) 

        localStorage.setItem(
            'cita',
            JSON.stringify(cita)
        )
    }; 

    const deleteCita= (id) =>{   
        const filterCita = cita.filter(cita => cita.id !== id); 
        setCita(  filterCita  ); 
    }; 
 



    return(
 
            <Container fluid className={clsx('mt-3')}>
                <Row>
                    <Col >
                        <HeaderVeterinaria title="Gestion de Citas Veterinaria" /> 
                    </Col> 
                </Row>
                <Row> 
                    <Col  >  
                        <FormCitas sendParent={createCita} />           
                    </Col>
                    <Col  >  
                        <DisplayVeterinaria title="Appointment Setted" data={cita} />           
                    </Col> 
                </Row> 
                <Row> 
                    <Col>  
                         <ListaVeterinaria title="Lista de citas" data={cita} handler={deleteCita}/>       
                    </Col>                    
                </Row>
                <Row> 
                    <Col>   
                        <ReturnHome/>   
                    </Col>                    
                </Row> 
                
            </Container>
 
    )
    
}

export default Veterinaria;
