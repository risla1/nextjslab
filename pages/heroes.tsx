import clsx from 'clsx';   
//  import { useState, useEffect } from 'react'; 
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'; 
 
import ReturnHome from '../components/commons/returnHome/returnHome';
import { Navbar } from '../components/commons/navbar/navbar';
import HeroList from '../components/heroes/heroList';
import { SearchScreen } from '../components/heroes/searchHero.tsx/searchScreen';
  


const Heroes = () => { 

 

    return(
        <Container fluid className={clsx('mt-3')}>
            <Row>
                <Col >
                    <Navbar />
                </Col> 
            </Row>  

           <hr/> 

            <Row>
                <Col>Buscador de Heroes</Col>
                <SearchScreen></SearchScreen>
            </Row>
                
           <hr/>
             
            <Row> 
                <Col>   
                    <ReturnHome/>   
                </Col>                    
            </Row> 
        </Container> 
    )
    
}

export default Heroes;
