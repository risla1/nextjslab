// import clsx from 'clsx'; 
import React from 'react';  


// class Sumador extends Component {
const Form1 = () => { 
  
    const [state, setState] = React.useState({
        email: '',
        password: ''
    });

    function handleInputChange(e) {
        setState({
          // spread in previous state with object spread operator
          ...state,
          [e.target.name]: e.target.value
        })
    }

     
    return(
        <form>

            <input
            name="email"
            type="email"
            onChange={handleInputChange}
            />

            <input
            name="password"
            type="password"
            onChange={handleInputChange}
            />

            
            <button type="submit">Submit</button>

            <hr></hr>
        </form>
    )
    
}

export default Form1;
