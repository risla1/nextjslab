import React, { useState, useCallback } from 'react'
import { Alert, Button, Row, Col, InputGroup, FormControl, Form } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import ContadorHijo from '../components/counter/contadorHijo';

const memoApp = () => {


    const [contador, setContador] = useState(0);



    // const sumar = () => setContador( contador +1); 
    
    // const restar = () => setContador(contador -1); 


    const sumar = useCallback(  () => {
        setContador( contador +1)
    }, [contador]);

    const restar = useCallback(  () => {
        setContador( contador -1)
    }, [contador]);
    






    const [input, setInput] = useState("");

    const handleSearch = (e) => {
        e.preventDefault(); 
        console.log("submit")
    }

    const handleInputChange = ( e) => {
        setInput(e.target.value)
        console.log(e.target.value) 
    } ; 

  return (
    <Container>
        <Row>
            <Alert>Contador</Alert>
        </Row>

        <Row>
            <Form onSubmit={handleSearch}>
            
                <InputGroup className="mb-3">

                    <FormControl
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2" 
                        name="searchText"
                        autoComplete="off"  
                        placeholder= "Search hero"
                        value={input}
                        onChange={ handleInputChange }        
                    />

                    <Button type="submit" variant="outline-secondary" id="button-addon2">
                        Search
                    </Button>

                </InputGroup>

            </Form>
        </Row>




        <Row className="d-flex flex-row justify-content-between flex-nowrap">
            <Col>
                <Button variant="success" onClick={sumar}>+</Button>
                <Button variant="info" onClick={restar}>-</Button>
            </Col>
        </Row>
        <hr/>
        <Row>Count:{contador}</Row>
        <hr/>
        <hr/>

        <Row>
            <ContadorHijo title="Contador Hijo" contador={contador} sumar={sumar} restar={restar}  />
        </Row>

    </Container>
  )
}

export default memoApp;