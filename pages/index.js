/* eslint-disable react/react-in-jsx-scope */
import Head from "next/head";
import Link from "next/link";
import Posts from "./posts";
import Header from "../components/header";
import Footer from "../components/footer";
import CounterApp from "../components/counterApp/counterApp";
import SlidersComp from "../components/SlidersComp/SlidersComp";
import "swiper/css/bundle";
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
// import 'bootstrap/dist/js/bootstrap.min.js'; 

export default function Home() {
  const personas = [
    { key: 1, nombre: "Euler", age: 25, city: "Lima" },
    { key: 2, nombre: "Diego", age: 25, city: "Tarapoto" },
  ];

  return (
    <Container className="container">
      <Head className="header">
        <title> ejercicios </title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
      <Header title=" NextJs Page Excercises" />

        <ul>
          <li>
            <Link href="/contact">
              <a> Ir a Contact </a>
            </Link>
          </li>
          <li>
            <Link href="/memoApp">
              <a> Ir a MemoApp </a>
            </Link>
          </li>

          <li>
            <Link href="/calculator/calculator">
              <a> Ir a Calculadora </a>
            </Link>
          </li>

          <li>
            <Link href="/onCLick">
              <a> Ir a onCLick change </a>
            </Link>
          </li>

          <li>
            <Link href="/incremento">
              <a> onCLick++ </a>
            </Link>
          </li>

          <li>
            <Link href="/form1">
              <a> Ir a formulario 1 </a>
            </Link>
          </li>

          <li>
            <Link href="/veterinaria">
              <a> Ir a Gestion de Citas</a>
            </Link>
          </li>

          <li>
            <Link href="/seguros">
              <a> Ir a Cotizador de Seguros</a>
            </Link>
          </li>

          <li>
            <Link href="/heroes">
              <a> Ir a App de Heroes</a>
            </Link>
          </li>
        </ul>


        <CounterApp value="32" />

        <Container className="grid">
          <h3> Posts </h3>
          <Posts posts={personas}> </Posts>
        </Container>

        <Container className="container-slide">
          <SlidersComp></SlidersComp>
        </Container> 

      </main>

      <footer>
        <Footer title="This it's the footer " />
      </footer>
    </Container>
  );
}
