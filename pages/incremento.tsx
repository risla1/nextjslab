// import clsx from 'clsx'; 
import React, { useState } from 'react';  


// class Sumador extends Component {
const Incremento = () => { 

    const [count, setCount] = useState(0);
 
    return(
        < >             
            <h2> {count} </h2>           
            
            <button   onClick={ () => setCount(count-1)}>Decremento -1</button>

            <button   onClick={ e => setCount(0) }>Reset</button>

            <button   onClick={e => setCount(count+1)}>Incrementar +1</button>  

            <hr></hr>
            
        </>
    )
    
}

export default Incremento;
