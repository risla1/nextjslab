import Link from 'next/link'


function Posts({ posts }) {

 
    
  return (
    <ul>
     {posts.map((post) => (
        <li key={post.key}>
          <Link href={`/blog/${encodeURIComponent(post.nombre)}`} >
            <a>{post.nombre}</a>
          </Link>
        </li>
      ))}  
    </ul>
  )
}

export default Posts