import * as React from 'react';
import type { AppProps } from 'next/app';
import clsx from 'clsx';
import ThemeProvider from 'react-bootstrap/ThemeProvider'
// import { ToastProvider } from 'react-toast-notifications';
// import { Provider } from 'next-auth/client';
// import { ThemeContext, ThemeType } from 'context';
// import { QueryClientProvider, QueryClient } from 'react-query';
import '../styles/tailwind.css';
import '../styles/globals.scss';  
import '../styles/fonts/fonts.scss';
import 'animate.css';

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
	// const [theme, setTheme] = React.useState<ThemeType>('light');
	// const queryClientRef = React.useRef<QueryClient | null>(null);

	// if (!queryClientRef.current) {
	// 	queryClientRef.current = new QueryClient();
	// }

	const personas =  [
		{key:1, nombre: "Euler", age: 25, city: "Lima" },
		{key:2, nombre: "Diego", age: 25, city: "Tarapoto" }
	  ];
	
	
	
	  return (
 
			<ThemeProvider
				className={clsx(
					'font-montserrat min-h-screen text-gray-800',
					'transition-colors duration-1000', 
				)}
			>
				<Component {...pageProps} />
			</ThemeProvider>
			 
	  )
}

export default MyApp;
