import clsx from 'clsx';    
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';  
import HeaderSeguros from '../components/seguros/headerSeguros/HeaderSeguros';
import FormSeguros from '../components/seguros/formularioSeguros/FormSeguros';
import DisplayCotizacion from '../components/seguros/displayCotizacion/displayCotizacion';
import {calcularMarca, diffYears, obtenerPlan} from '../helpers/cotizador';
import { useState } from 'react';
import { Card } from 'react-bootstrap';
import ReturnHome from '../components/commons/returnHome/returnHome';

const CotizadorSeguros = () => { 

      
    const Brands = [
 
        { 
          'id'     : 0,   
          'brand' : 'Brand select' 
        }, 
        { 
          'id'     : 1,   
          'brand' : 'Europeo' 
        }, 
        { 
          'id'     : 2,   
          'brand' : 'Americano' 
        },
        { 
          'id'     : 3,   
          'brand' : 'Asiatico' 
        }, 
        // { 
        //   'id'     : 4,   
        //   'brand' : 'Hyundai' 
        // }, 
        // { 
        //   'id'     : 5,   
        //   'brand' : 'Daewoo' 
        // }, 
        // { 
        //   'id'     :6,   
        //   'brand' : 'Chery' 
        // }, 
       
    ];

    const Years = [
        
        { 
            'id'     : 0,   
            'year' : 'Year Select' 
        }, 
        // { 
        //     'id'     : 1,   
        //     'year' : '1950' 
        // }, 
        // { 
        //     'id'     : 2,   
        //     'year' : '1960' 
        // }, 
        // { 
        //     'id'     : 3,   
        //     'year' : '1980' 
        // },
        // { 
        //     'id'     : 4,   
        //     'year' : '1990' 
        // }, 
        // { 
        //     'id'     : 5,   
        //     'year' : '1995' 
        // }, 
        { 
            'id'     : 1,   
            'year' : '2018' 
        }, 
        { 
            'id'     : 2,   
            'year' : '2019' 
        },
        { 
            'id'     : 3,   
            'year' : '2020' 
        }, 
        { 
            'id'     : 4,   
            'year' : new Date().getFullYear() 
        }, 
        
    ]; 


     
    const [datas, setDatas] = useState({
        resultado :'',
        datos: {}
    })


    const crearCotizacion= (data) =>{  
        const {brand, year, plan} = data; 

        let resultado = 2000; 
        let diferencia = diffYears(year); 

        resultado -= ((diferencia * 3) * resultado) /100;
        resultado = calcularMarca(brand) *resultado ;
         
        let incrementoPlan  = obtenerPlan(plan)

        resultado =  Number(incrementoPlan * resultado) ;  
        

        const datosAuto = {
            brand: brand,
            year: year,
            plan: plan,           
        }



        setDatas({
            resultado: String(resultado),
            datos:datosAuto
        }); 
 
    };  

 

    return(
 
            <Card   className={clsx('mt-3')}>
                <Card.Header>
                    <Col >
                        <HeaderSeguros title="Cotizador de Seguros" /> 
                    </Col> 
                </Card.Header> 
                <Card.Body> 
                    <Col >
                        {/* Aqui le mando los arrays de años y marcas disponibles y obtengo la data del formulario en --crearCotizacion-- */}
                        <FormSeguros title="Cotizador de Seguros" years={Years} brands={Brands} sendParent={crearCotizacion}/> 
                    </Col>  
                </Card.Body>
                <Card.Footer> 
                    <Col >
                        <DisplayCotizacion title="Cotización Hecha" resultado={datas.resultado} info={datas.datos}/> 
                    </Col>  
                </Card.Footer>
                <Container>
                    <ReturnHome/>
                </Container>                
            </Card>
 
    )
    
}

export default CotizadorSeguros;
